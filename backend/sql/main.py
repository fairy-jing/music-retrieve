from queue import Empty
from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()
# uvicorn sql.main:app --reload

# Dependency


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/songs/", response_model=schemas.Song)
def create_song(song: schemas.SongCreate, db: Session = Depends(get_db)):
    return crud.create_song(db=db, song=song)

# 按图片检索


@app.post("/songspicture/", response_model=list[schemas.Song])
def get_songs_by_picture(picture: schemas.ImageBase, db: Session = Depends(get_db)):
    # songs=crud.get_song_by_picture(db=db, picture=picture.images)
    st = crud.get_song_by_picture(db=db, picture=picture.images)
    songs = crud.get_song_by_lyric(db=db, lyric=st)
    return songs

# 按音频检索


@app.post("/songsspeech/", response_model=list[schemas.Song])
def get_songs_by_speech(speech: schemas.SpeechBase, db: Session = Depends(get_db)):
    # songs=crud.get_song_by_speech(db=db, speech=speech.speech)
    st = crud.get_song_by_speech(db=db, speech=speech.speech)
    songs = crud.get_song_by_lyric(db=db, lyric=st)
    return songs


@app.get("/songs/", response_model=list[schemas.Song])
def get_songs(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    songs = crud.get_songs(db, skip=skip, limit=limit)
    return songs


@app.get("/songs/{song_id}", response_model=schemas.Song)
def get_song(song_id: int, db: Session = Depends(get_db)):
    db_song = crud.get_song(db, id=song_id)
    if db_song is None:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_song


@app.get("/singer/{singer}", response_model=list[schemas.Song])
def get_song_by_singer(singer: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_singer(db, singer=singer)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_songs


@app.get("/composer/{composer}", response_model=list[schemas.Song])
def get_song_by_composer(composer: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_composer(db, composer=composer)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_songs


@app.get("/arranger/{arranger}", response_model=list[schemas.Song])
def get_song_by_arranger(arranger: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_arranger(db, arranger=arranger)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_songs


@app.get("/lyricist/{lyricist}", response_model=list[schemas.Song])
def get_song_by_lyricist(lyricist: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_lyricist(db, lyricist=lyricist)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_songs


@app.get("/Rdate/{Rdate}", response_model=list[schemas.Song])
def get_song_by_Rdate(Rdate: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_Rdate(db, Rdate=Rdate)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_songs


@app.get("/lang/{lang}", response_model=list[schemas.Song])
def get_song_by_lang(lang: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_lang(db, lang=lang)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="Song not found")
    return db_songs


# 歌曲名模糊搜索
@app.get("/songnames/{name}", response_model=list[schemas.Song])
def get_song_by_name(name: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_name(db, name=name)
    if db_songs == []:
        raise HTTPException(status_code=404, detail="未查询到相关的歌曲")
    return db_songs

# 歌词模糊搜索


@app.get("/lyric/{song_lyric}", response_model=list[schemas.Song])
def get_song_by_lyric(song_lyric: str, db: Session = Depends(get_db)):
    db_songs = crud.get_song_by_lyric(db, lyric=song_lyric)
    if not db_songs:
        raise HTTPException(status_code=404, detail="未查询到与当前歌词相关的歌曲")
    return db_songs


# 删除
@app.delete("/songs/{song_id}")
def delete_song(song_id: int, db: Session = Depends(get_db)):
    return crud.delete_song(db, song_id=song_id)

# 更新


@app.post("/updateSongs/{song_id}", response_model=schemas.Song)
def update(song_id: int, song: schemas.SongBase, db: Session = Depends(get_db)):
    return crud.updata_song(db, song_id=song_id, song=song)
