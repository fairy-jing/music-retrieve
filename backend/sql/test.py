import requests
import json
import cv2
import base64
import jieba
import cv2
# 命令行预测
# hub run ch_pp-ocrv3 --input_path ".\2022-11-06 104645.jpg"

# 代码示例
# ocr = hub.Module(name="ch_pp-ocrv3", enable_mkldnn=True)       # mkldnn加速仅在CPU下有效
# result = ocr.recognize_text(images=[cv2.imread('G:\\music-retrieve-master\\backend\\sql\\6.jpg')])
# print(result.json())


def cv2_to_base64(image):
    data = cv2.imencode('.jpg', image)[1]
    return base64.b64encode(data.tobytes()).decode('utf8')
#[cv2_to_base64(cv2.imread("sql\\2022-11-06 104645.jpg"))]


# 服务部署
# 启动命令 hub serving start -m ch_pp-ocrv3
# 发送HTTP请求
s = requests.session()
s.keep_alive = False
data = {'images': cv2_to_base64(cv2.imread(
    r"C:\Users\CandyLynch\Desktop\music-retrieve-master\backend\sql\6.jpg"))}
headers = {"Content-type": "application/json"}
url = "http://127.0.0.1:8000/songspicture"
r = requests.post(url=url, headers=headers, data=json.dumps(data))
print(r)
# 打印预测结果

# sentence=r.json()["results"][0]["data"]
# sentence.sort(key=lambda x:x['confidence'],reverse=True)
# st=''
# for key in sentence[:5]:
#    st+=key["text"]
# print(st)
# tokens= list(jieba.cut_for_search(st)  )
# print(tokens)


# wav_file = "sql\out.wav"
# asr_url = "http://222.199.232.45:8880/paddlespeech/asr"
# # 将wav转成base64
# with open(wav_file, 'rb') as f:
#     base64_bytes = base64.b64encode(f.read())
#     base64_string = base64_bytes.decode('utf-8')

# data = {
#     "speech": base64_string,
# }
# headers = {"Content-type": "application/json"}
# url = "http://127.0.0.1:8000/songsspeech"
# r = requests.post(url=url, headers=headers, data=json.dumps(data))

# print(r.json())
