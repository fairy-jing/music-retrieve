from email.policy import default
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base

class Song(Base):
    __tablename__="songs"
    id = Column(Integer, primary_key=True, index=True)
    name=Column(String)#歌曲名，默认为空
    singer=Column(String) #歌手，默认是VA也就是群星的意思
    composer=Column(String)#作曲、谱曲，默认是VA也就是群星的意思
    arranger=Column(String)#编曲，默认是VA也就是群星的意思
    lyricist=Column(String)#填词，默认是VA也就是群星的意思
    Rdate=Column(String)#发行日期，默认为空
    lyric=Column(String)#歌词，默认为空
    lang=Column(String)#歌词语言默认为中文
