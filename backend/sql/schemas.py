from pydantic import BaseModel


class SongBase(BaseModel):
    name:str
    singer:str
    composer:str
    arranger:str
    lyricist:str
    Rdate:str
    lyric:str
    lang:str

class ImageBase(BaseModel):
    images:str
    class Config:
        orm_mode = True

class SpeechBase(BaseModel):
    speech:str
    class Config:
        orm_mode = True

class SongCreate(SongBase):
    class Config:
        orm_mode = True


class Song(SongBase):
    id: int
    class Config:
        orm_mode = True
