from sqlalchemy.orm import Session
import jieba
from . import models, schemas
from sqlalchemy import create_engine
import requests
import json
import cv2
import base64

engine = create_engine("sqlite:///./sql_app.db")

# 检索


def get_song(db: Session, id: int):
    return db.query(models.Song).filter(models.Song.id == id).first()


def get_song_by_name(db: Session, name: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.name.like(f"%{name}%")).all()


def get_song_by_singer(db: Session, singer: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.singer == singer).limit(limit).all()


def get_song_by_composer(db: Session, composer: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.composer == composer).limit(limit).all()


def get_song_by_arranger(db: Session, arranger: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.arranger == arranger).limit(limit).all()


def get_song_by_lyricist(db: Session, lyricist: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.lyricist == lyricist).limit(limit).all()


def get_song_by_Rdate(db: Session, Rdate: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.Rdate == Rdate).limit(limit).all()


def get_song_by_lang(db: Session, lang: str, limit: int = 100):
    return db.query(models.Song).filter(models.Song.lang == lang).limit(limit).all()


def get_songs(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Song).offset(skip).limit(limit).all()


# 模糊搜索歌词
def get_song_by_lyric(db: Session, lyric: str):
    # return db.query(models.Song).filter(models.Song.lyric.like(f"%{lyric}%")).all()
    tokens = list(jieba.cut_for_search(lyric))
    print("搜索引擎模式:" + "| ".join(tokens))
    sql = ''
    if len(tokens) < 1:
        return None
    sql1 = "SELECT *,("+("(CASE WHEN( lyric LIKE \"%" +
                         tokens[0]+"%\") THEN 1  ELSE 0 END )")
    regexp = "\""+tokens[0]
    for t in tokens[1:]:
        sql1 += ("+(CASE WHEN( lyric LIKE \"%"+t+"%\") THEN 1  ELSE 0 END )")
        regexp += ("|"+t)
    sql1 += ") AS keyweight FROM songs WHERE lyric REGEXP "
    regexp += '\" '
    sql1 += regexp
    sql1 += "ORDER BY keyweight DESC"
    # print(sql1)
    # print("-----------------")
    with engine.connect() as conn:
        res = conn.execute(sql1).fetchall()

    return res


# 图片歌词识别并搜索
def get_song_by_picture(db: Session, picture: str):
    # 发送HTTP请求
    data = {'images': [picture]}
    headers = {"Content-type": "application/json"}
    url = "http://222.199.232.45:8866/predict/ch_pp-ocrv3"
    r = requests.post(url=url, headers=headers, data=json.dumps(data))
    # 打印预测结果
    # print(r.json()["results"])
    sentence = r.json()["results"][0]["data"]
    sentence.sort(key=lambda x: x['confidence'], reverse=True)
    st = sentence[0]["text"]
    # return db.query(models.Song).filter(models.Song.lyric.like(f"%{st}%")).all()
    # return get_song_by_lyric(lyric=st)
    return st


# 语音歌词识别并搜索
def get_song_by_speech(db: Session, speech: str):
    # 发送HTTP请求
    #data = {'speech':[speech]}
    data = {
        "audio": speech,
        "audio_format": "wav",
        "sample_rate": 16000,
        "lang": "zh_cn",
        "punc": 0
    }

    headers = {"Content-type": "application/json"}
    url = "http://222.199.232.45:8880/paddlespeech/asr"
    r = requests.post(url=url, headers=headers, data=json.dumps(data))
    # 打印预测结果
    # print(r.json()["results"])
    st = r.json()['result']["transcription"]
    return st
    # return db.query(models.Song).filter(models.Song.lyric.like(f"%{st}%")).all()


# 增加歌曲
def create_song(db: Session, song: schemas.SongCreate):
    db_song = models.Song(name=song.name, singer=song.singer, composer=song.composer,
                          arranger=song.arranger, lyricist=song.lyricist, Rdate=song.Rdate, lyric=song.lyric, lang=song.lang)
    db.add(db_song)
    db.commit()
    db.refresh(db_song)
    return db_song

# 删除歌曲


def delete_song(db: Session, song_id: int):
    db_song = db.query(models.Song).filter(
        models.Song.id == song_id).one_or_none()
    if db_song is None:
        return None
    db.delete(db_song)
    db.commit()
    return True

# 根据歌曲id修改歌曲信息


def updata_song(db: Session, song_id: int, song: schemas.SongBase):
    db_song = db.query(models.Song).filter(
        models.Song.id == song_id).one_or_none()
    if db_song is None:
        return None

    # updata
    for var, value in vars(song).items():
        setattr(db_song, var, value) if value else None
    db.commit()
    db.refresh(db_song)
    return db_song
