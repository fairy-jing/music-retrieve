# 歌曲检索APP
## 1 项目架构（参考）
#### 1.1 后端（难点是歌词模糊搜索）：
  * 语言：Python
  * 框架：[Fastapi](https://fastapi.tiangolo.com/zh/)
  * 数据库：mysql、postgresql或者mongodb（文档型数据库）
  * 图片歌词识别：[PaddleOCR](https://github.com/PaddlePaddle/PaddleOCR/blob/release/2.6/README_ch.md)
  * 歌曲语音转文字：[PaddleSpeech](https://github.com/PaddlePaddle/PaddleSpeech/blob/develop/README_cn.md)
  * [可选]歌曲专辑图片存储：百度图床
#### 1.2 前端：
  * 语言：Python
  * 框架：[Streamlit](https://docs.streamlit.io/)
#### 1.3 客户端：
  * windows+Linux：[Electron](https://www.electronjs.org/zh/docs/latest/tutorial/quick-start)
  * 安卓（如果web前端适配移动端的话）：各种一键打包网站或者工具
#### 1.4 歌曲信息：
  * 爬虫：自己学和入手，这样每个人都有可以参与的了。
  * 人工：自己喜欢的添加进去
  * 歌曲信息网站：百度百科、维基百科、各种音乐网站
## 2 后端操作
####　2.1 fastapi
  *  [相关教程](https://fastapi.tiangolo.com/zh/tutorial/sql-databases/)
  *  安装见教程，python版本为3.10
  *  启动：
    cd backend
    uvicorn sql.main:app --reload
  * api文档：http://127.0.0.1:8000/docs#/ 启动之后访问
## 3 前端操作
  * python: 3.10
  * 启动：
    pip install streamlit [optional]
    cd frontend 
    streamlit run search_a_song.py