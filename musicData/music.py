# -*- coding = utf-8 -*-
# @Time :2022/11/12 23:46
# @Author:YeeBy
# @File : music.py
# @Software: PyCharm
import random
import sqlite3
import time
import urllib.request

import pandas
from bs4 import BeautifulSoup
import csv

def open_url(url):
    req = urllib.request.Request(url)
    # 模拟电脑访问
    req.add_header("User-Agent",
                   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36")
    try:
        response = urllib.request.urlopen(req)
    except:
        return "404"
    # code = response.getcode()
    # print(code)
    # print(type(code))
    # errorCode = 404
    # if code == errorCode:
    #     print("haahah")
    #     return "404"
    html = response.read()

    # 将源码以utf-8 的形式进行解码
    return html.decode("utf-8")


def find_attribute(url) -> str:
    html = open_url(url)
    if html == "404":
        return "404"
    soup = BeautifulSoup(html, 'html.parser')

    # musics = soup.select("h1.data__name_txt")[0].next_element
    # print(musics)
    # # 歌手
    # singers = soup.select("div.data__singer > a")[0].next_element
    # print(singers)
    # # # 专辑
    # albums = soup.select("li.data_info__item_song > a")[0].next_element
    # print(albums)
    #
    # albums = soup.select("li.data_info__item_song")
    # lang = str(albums).split("<!-- -->")[1].split("</")[0]
    # print(lang)
    #
    # date = str(albums).split("发行时间：<!-- -->")[1].split("</")[0]
    # print(date)

    musics = soup.select("span.song-detail-name")[0].next_element
    # print(musics)
    singer = soup.select("p.singer > span > a")[0].next_element
    # print(singer)
    album = soup.select("p.album > a")[0].next_element
    # print(album)
    date = soup.select("p.area > span")[0].next_element
    # print(date)
    lyrics = soup.select("div.page-content > div.page-left")
    lyrics = str(lyrics).split("<ul class=\"lyric clearfix\">")[1].split("</ul>")[0]
    lines = lyrics.split("<li>")
    ly = ""

    encount = 0
    jpcount = 0
    krcount = 0
    jptotal = 0
    krtotal = 0
    for line in lines:
        cur = line.split("<")[0]
        if cur != "" and cur != "\n":
            #        print(cur)
            cur = cur.split("\n")[0]
            ly += cur+"\n"
            if cur.__contains__("g") or cur.__contains__("d"):
                encount += 1
            ran = random.randint(0,100)
            if ran >=80:
                jptotal += 1
                for c in cur:
                    if c >= '\u3040' and c <= '\u309f':
                        jpcount += 1
                        break
            if ran >= 60:
                krtotal += 1
                for c in cur:
                    if c >= '\uac00'and c <= '\ud7a3':
                        krcount += 1
                        break

    if "暂无" in lyrics:
        return [musics, singer, album, date, ly, "composer", "lyricist", "lang"]
    try:
        composer = lyrics.split("曲")[1].split("</")[0]
        if composer.__contains__("：") or composer.__contains__(":"):
            composer = composer.split(":")[1] if composer.split(":").__len__()>1 else composer.split("：")[1]
        # print(composer)
        lyricist = lyrics.split("词")[1].split("</")[0]
        if lyricist.__contains__("：") or lyricist.__contains__(":"):
            lyricist = lyricist.split(":")[1] if lyricist.split(":").__len__()>1 else lyricist.split("：")[1]
        # print(lyricist)
        # print(encount,lines.__len__())
        lang = "英语" if ((0.0+encount)/lines.__len__()) >= 0.6 else "日语" if ((0.0+jpcount)/jptotal) >= 0.6 else "韩语" if ((0.0+krcount/krtotal)) >= 0.6 else "华语"
    except:
        return "404"
    print(ly)
    return [musics, singer, album, date, ly, composer, lyricist, lang]


def main():

    # url = list()
    # urlId = 12616748
    
    # for i in range(1,100):
    #     url.append("https://www.igeciku.com/song/"+str(urlId)+".html")
    #     urlId = urlId+1
    
    # # url = ["https://www.igeciku.com/song/12616756.html"]
    
    # with open('D:/music.csv', 'w', encoding="utf-8-sig",newline='') as file:
    #     writer = csv.writer(file)
    #     # writer.writerow(["id","name", "singer", "album", "date", "line", "composer", "lyricist", "lang"])
    #     writer.writerow(["id","name", "singer", "composer", "arranger", "lyricist", "date", "lyric", "lang"])
    #     cnt = 1
    #     for u in url:
    #         #randomTime = random.randint(1,10)
    #         print(u)
    #         c = find_attribute(u)
    #         if c == "404":
    #             continue
    #         print(c[7])
    #         writer = csv.writer(file)
    #         # name singer album date line composer lyricist lang
    #         writer.writerow([cnt,c[0], c[1], c[5], c[5], c[6], c[3], c[4], c[7]])
    #         cnt = cnt+1
    #         #time.sleep(randomTime)

    conn = sqlite3.connect("backend\sql_app.db")
    cur = conn.cursor()
    # cmd = 'select * from songs;'
    # cur.execute(cmd)
    # values = cur.fetchall()
    # print(values)
    df = pandas.read_csv('G:\music-retrieve-master\musicData\music.csv')
    df.to_sql('songs', conn, if_exists='append', index=False)
    print('ok')
    conn.close()

if __name__ == "__main__":
    main()
