import streamlit as st
import requests
import json


st.markdown("# Search a song 😊")
st.sidebar.markdown("# Search a song 😊")
st.sidebar.write("Input some content to start searching !")

option = st.selectbox(
    'What do you want to search by?',
    ('Song Name', 'Singer', 'Composer', 'Arranger', 'Lyricist', 'Date', 'Language'))

st.write('You chose to search by ', option, ', please input the content:')
content = st.text_input(option)
info = None
data = None
if st.button('Search'):
    if option == 'Song Name':
        url = "http://127.0.0.1:8000/songnames/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})
    if option == 'Singer':
        url = "http://127.0.0.1:8000/singer/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})
    if option == 'Composer':
        url = "http://127.0.0.1:8000/composer/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})
    if option == 'Arranger':
        url = "http://127.0.0.1:8000/arranger/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})
    if option == 'Lyricist':
        url = "http://127.0.0.1:8000/lyricist/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})
    if option == 'Date':
        url = "http://127.0.0.1:8000/Rdate/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})
    if option == 'Language':
        url = "http://127.0.0.1:8000/lang/"
        data = requests.get(
            url=url+content, headers={"ContentType": "application/json"})

# st.write(data)
# st.write(data.content.decode("utf-8"))
if data is None or data.status_code == 404:
    st.markdown("### Song's not found - maybe next time 🤗")
else:
    data = json.loads(data.content.decode(data.encoding))
    for item in data:
        with st.expander(item['name']):
            st.markdown("#### 歌手：")
            st.write(item['singer'])
            st.markdown("#### 作曲：")
            st.write(item['composer'])
            st.markdown("#### 编曲：")
            st.write(item['arranger'])
            st.markdown("#### 作词：")
            st.write(item['lyricist'])
            st.markdown("#### 发行日期：")
            st.write(item['Rdate'])
            st.markdown("#### 语言：")
            st.write(item['lang'])
            st.markdown("#### 歌词：")
            for lyric in item['lyric'].split('\n'):
                st.write(lyric)
