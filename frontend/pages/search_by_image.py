import streamlit as st
import requests
import json
import cv2
import base64


def cv2_to_base64(image):
    data = cv2.imencode('.jpg', image)[1]
    return base64.b64encode(data.tostring()).decode('utf8')


st.markdown("# Search by image 🥳")
st.sidebar.markdown("# Search by image 🥳")
st.sidebar.write("Try amazing image searching !")

st.write("Upload an image to search songs:")
uploaded_file = st.file_uploader("Choose a file")
if uploaded_file is not None:
    byte_data = uploaded_file.read()
    data = {'images': base64.b64encode(byte_data).decode('utf8')}
    url = "http://127.0.0.1:8000/songspicture/"
    info = requests.post(
        url=url, headers={"ContentType": "application/json"}, data=json.dumps(data))
    # st.write(info)
    info = json.loads(info.content.decode(info.encoding))
    for item in info:
        with st.expander(item['name']):
            st.markdown("#### 歌手：")
            st.write(item['singer'])
            st.markdown("#### 作曲：")
            st.write(item['composer'])
            st.markdown("#### 编曲：")
            st.write(item['arranger'])
            st.markdown("#### 作词：")
            st.write(item['lyricist'])
            st.markdown("#### 发行日期：")
            st.write(item['Rdate'])
            st.markdown("#### 语言：")
            st.write(item['lang'])
            st.markdown("#### 歌词：")
            for lyric in item['lyric'].split('\n'):
                st.write(lyric)
