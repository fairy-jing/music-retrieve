import streamlit as st
import requests
import json

st.markdown("# View all songs 😉")
st.sidebar.markdown("# View all songs 😉")
st.sidebar.write("Enjoy exploring music everyday !")

st.markdown("Hi - These are all songs available in our website 🥰:")
url = "http://127.0.0.1:8000/songs/"
data = requests.get(
    url=url, headers={"ContentType": "application/json"})


if data is None or data.status_code == 404:
    st.markdown("### Something's wrong with network - maybe next time 🤗")
else:
    data = json.loads(data.content.decode(data.encoding))
    for item in data:
        with st.expander(item['name']):
            st.markdown("#### id：")
            st.write(item['id'])
            st.markdown("#### 歌手：")
            st.write(item['singer'])
            st.markdown("#### 作曲：")
            st.write(item['composer'])
            st.markdown("#### 编曲：")
            st.write(item['arranger'])
            st.markdown("#### 作词：")
            st.write(item['lyricist'])
            st.markdown("#### 发行日期：")
            st.write(item['Rdate'])
            st.markdown("#### 语言：")
            st.write(item['lang'])
            st.markdown("#### 歌词：")
            for lyric in item['lyric'].split('\n'):
                st.write(lyric)
