import streamlit as st
import requests
import json


st.markdown("# Search by lyrics 😜")
st.sidebar.markdown("# Search by lyrics 😜")
st.sidebar.write("Try amazing lyrics fuzzy searching !")

lyrics = st.text_input('Song lyrics')
st.write('Songs contain lyrics \' ', lyrics, ' \':')
url = "http://127.0.0.1:8000/lyric/"
data = requests.get(
    url=url+lyrics, headers={"ContentType": "application/json"})
if data is None or data.status_code == 404:
    st.markdown("### Song's not found - maybe next time 🤗")
else:
    data = json.loads(data.content.decode(data.encoding))
    for item in data:
        with st.expander(item['name']):
            st.markdown("#### 歌手：")
            st.write(item['singer'])
            st.markdown("#### 作曲：")
            st.write(item['composer'])
            st.markdown("#### 编曲：")
            st.write(item['arranger'])
            st.markdown("#### 作词：")
            st.write(item['lyricist'])
            st.markdown("#### 发行日期：")
            st.write(item['Rdate'])
            st.markdown("#### 语言：")
            st.write(item['lang'])
            st.markdown("#### 歌词：")
            for lyric in item['lyric'].split('\n'):
                st.write(lyric)
