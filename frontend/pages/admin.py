import streamlit as st
import requests
import json
import cv2
import base64

st.markdown("# Admin 🤠")
st.sidebar.markdown("# Admin 🤠")
st.sidebar.write("Manage all songs !")

st.write("Please input password to confirm your identity: ")
password = st.text_input('Password')

if password == '123456':  # password
    col1, col2 = st.columns(2)

    with col1:
        option = st.selectbox(
            'What do you want to do?',
            ('Delete a song', 'Create a song', 'Update a song'))

    with col2:
        if option == 'Delete a song':
            st.write("Input id you want to delete: ")
            song_id = st.text_input('Song id')
            url = "http://127.0.0.1:8000/songs/"
            if st.button('Delete'):
                data = requests.delete(
                    url=url+song_id, headers={"ContentType": "application/json"})
                if data.status_code == 200:
                    st.write("Song ", song_id, " is deleted 🤗! ")
                else:
                    st.write(data.status_code)
                    st.write("Ops! Something went wrong - try next time 🤗")
        if option == 'Create a song':
            st.write("Input song information: ")
            new_song = {}
            new_song['name'] = st.text_input('name')
            new_song['singer'] = st.text_input('singer')
            new_song['composer'] = st.text_input('composer')
            new_song['arranger'] = st.text_input('arranger')
            new_song['lyricist'] = st.text_input('lyricist')
            new_song['Rdate'] = st.text_input("date")
            new_song['lyric'] = st.text_input("lyrics")
            new_song['lang'] = st.text_input("language")
            if st.button('Create'):
                url = "http://127.0.0.1:8000/songs/"
                data = requests.post(
                    url=url, headers={"ContentType": "application/json"}, data=json.dumps(new_song))
                if data.status_code == 200:
                    st.write("Song ", new_song['name'], " is created 🤗! ")
                else:
                    st.write("Ops! Something went wrong - try next time 🤗")
        if option == 'Update a song':
            st.write("Input id you want to update: ")
            song_id = st.text_input('Song id')
            st.write("Input new song information: ")
            new_song = {}
            new_song['name'] = st.text_input('name')
            new_song['singer'] = st.text_input('singer')
            new_song['composer'] = st.text_input('composer')
            new_song['arranger'] = st.text_input('arranger')
            new_song['lyricist'] = st.text_input('lyricist')
            new_song['Rdate'] = st.text_input("date")
            new_song['lyric'] = st.text_input("lyrics")
            new_song['lang'] = st.text_input("language")
            url = "http://127.0.0.1:8000/updateSongs/"
            if st.button('Update'):
                data = requests.post(
                    url=url+song_id, headers={"ContentType": "application/json"}, data=json.dumps(new_song))
                if data.status_code == 200:
                    st.write("Song ", song_id, " is updated 🤗! ")
                else:
                    st.write(data.status_code)
                    st.write("Ops! Something went wrong - try next time 🤗")
